#!/bin/sh
if [ -z "$1" ]; then
    if [ ! -f "/config/content.pug" ]; then
        cp "content.template.pug" "/config/content.pug"
    fi
    ln -sf "/config/content.pug" "content.pug"

    yarn build
    caddy run
else
    exec $@
fi
