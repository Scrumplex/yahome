yahome
------
yet another homepage for your self-hosted services


# setup

yahome just generates a static webpage once, that could be hosted multiple ways.
my workflow builds on containers, which is why yahome is provided as [a container on Docker Hub](https://hub.docker.com/r/scrumplex/yahome) (and other registries soon).

in any case the process is as follows: get yahome -> create or generate a config -> build -> serve
in case of the container steps three and four are completely handled by the container.

## docker

run the container once:

[example docker-compose.yaml](examples/docker-compose.yaml)

```
# docker run -p 8080:8080 -v /srv/yahome-config:/config docker.io/scrumplex/yahome:latest

$ podman run -p 8080:8080 -v /srv/yahome-config:/config docker.io/scrumplex/yahome:latest
```
edit the newly generated `/srv/yahome-config/content.pug` to your liking

restart the server
done!

## manual

1. clone this repository
2. create a copy of [content.template.pug](content.template.pug) and call it `content.pug` in the project directory
3. edit the newly created copy to your liking
4. run `yarn install` and `yarn build`
5. serve the files that were generated into `dist/`

```
$ git clone https://git.sr.ht/~scrumplex/yahome
$ cp content.template.pug content.pug
(edit content.pug)
$ yarn install
$ yarn build
(serve files in dist/)
```

